/*
 * Generate an SQL script to create an entity based on the given model.
 */

const USAGE_MESSAGE = `USAGE: node genSQL ModelName`;

if (process.argv.length !== 3) {
  console.error(USAGE_MESSAGE);
  process.exit(-1);
}

const modelName = process.argv[2];

let model;
try {
  model = require(`../models/${modelName}`);
} catch (err) {
  console.log(err);
  process.exit(-1);
}

console.log(model.toSQL());
