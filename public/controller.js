const CONTROLLER = document.getElementById('controller');
const ROWS = document.querySelectorAll('#controller tbody tr');
const EDIT_BTN = document.getElementById('edit-button');
const VIEW_BTN = document.getElementById('view-button');
const DELETE_BTN = document.getElementById('delete-button');
let PREVIOUSLY_SELECTED_ROW;

ROWS.forEach(row => {
  row.addEventListener('click', () => {
    setSelectedRow(row);
  });
});

function setSelectedRow(row) {
  const query = row.id;
  if (PREVIOUSLY_SELECTED_ROW) PREVIOUSLY_SELECTED_ROW.classList.remove('selected');
  row.classList.add('selected');
  PREVIOUSLY_SELECTED_ROW = row;
  CONTROLLER.classList.add('instance-selected');
  EDIT_BTN.setAttribute('href', `./edit?${query}`);
  VIEW_BTN.setAttribute('href', `./detail?${query}`);
  DELETE_BTN.setAttribute('href', `./delete?${query}`);
}
