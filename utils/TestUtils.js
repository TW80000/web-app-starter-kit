require('dotenv').config();
const { Pool } = require('pg');
const DB = new Pool();
const Model = require('../services/Model');

/**
 * Utilities for writing tests.
 */
class TestUtils {
  constructor() {}

  /**
   * Run a test.
   * @param {String} description A description of the test.
   * @param {Function} code The test code to run. Should return a boolean.
   */
  static async test(description, code) {
    try {
      const passed = await code();
      const prefix = passed ? '\x1b[32mPASSED' : '\x1b[31mFAILED';
      console.log(`${prefix}: ${description}\x1b[0m`);
    } catch (err) {
      console.log(`\x1b[31mERRORED: ${description}\x1b[0m`);
      console.error(err);
    }
  }

  /**
   * Return a modified copy of the given model with the table name changed so that any operations
   * done with the model and the database won't affect data in the real table. Also creates the new
   * testing table.
   * @param {Model} model A model.
   * @returns {Model} A modified copy of the given model with the table name changed.
   */
  static async mock(model) {
    var obj = model.toObject();
    obj.table.name = getMockTableName(model);
    var mockedModel = new Model(obj);
    await this.createTable(mockedModel);
    return mockedModel;
  }

  /**
   * Create a table for a given model.
   * @param {Model} model A model.
   */
  static async createTable(model) {
    try {
      await this.dropTable(model);
    } catch (err) {}
    await DB.query(model.toSQL());
  }

  /**
   * Drop the table for the given model.
   * @param {Model} model A model.
   */
  static async dropTable(model) {
    const tableName = model.toObject().table.name;
    await DB.query(`DROP TABLE ${tableName}`);
  }

  /**
   * Delete all records from the table for the given model.
   * @param {model} model A model.
   */
  static async clearTable(model) {
    const tableName = model.toObject().table.name;
    await DB.query(`DELETE FROM ${tableName}`);
  }
}

/**
 * Get a mock table name for a given model.
 * @param {Model} model A model.
 */
function getMockTableName(model) {
  return 'test_' + model.toObject().table.name;
}

module.exports = TestUtils;
