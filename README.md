# Web app starter kit

Basic starter kit for making a web app using the tech I know and like.

## Tech

DB: PostgresSQL

Web server: Express

Templating engine: Pug

### Environment variables

You'll need to create a `.env` file that contains the environment variables you want to use. They're loaded by the dotenv npm library.

## Tests

Tests can be run with `npm test`.

## Documentation

Docs are generated using `jsdoc`. Run `npm run docs` to generate them.
