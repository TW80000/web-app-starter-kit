const express = require('express');
const bodyParser = require('body-parser');
const pug = require('pug');
const dotenvResult = require('dotenv').config();
const { Pool } = require('pg');
const cookieParser = require('cookie-parser');
const User = require('./models/User.js');
const UserService = User.toService();

if (dotenvResult.error) throw dotenvResult.error;

const SERVER = express();
const PORT = 8081;

// Set up middleware.

SERVER.set('views', './views');
SERVER.set('view engine', 'pug');

SERVER.use(bodyParser.json());
SERVER.use(bodyParser.urlencoded({ extended: true }));

SERVER.use(cookieParser());

SERVER.use('*', async (req, res, next) => {
  try {
    req.isAuthenticated =
      req.cookies &&
      req.cookies.token &&
      (await UserService.select('session_token', req.cookies.token)).pop();
  } catch (err) {
    console.log(`Possibly invalid session token: ${req.cookies.token}`);
    console.error(err);
    req.isAuthenticated = false;
  }
  next();
});

function authenticate(req, res, next) {
  if (!req.isAuthenticated) {
    return res.redirect(`/login?redirectTo=${req.originalUrl}`);
  } else {
    next();
  }
}

SERVER.use('/', express.static('public'));
SERVER.use('/login', require('./routers/LoginRouter.js'));
SERVER.use('/logout', authenticate, require('./routers/LogoutRouter.js'));
SERVER.use('/register', require('./routers/RegisterRouter.js'));
SERVER.use('/api/users', User.toJsonRouter());
SERVER.use('/users', User.toViewRouter());

// Set up routes.

SERVER.get('/', (req, res, next) =>
  res.render('home', {
    base: process.env.base,
    isAuthenticated: req.isAuthenticated,
    banner: req.query.logout ? { message: 'Successfully logged out.', type: 'success' } : undefined,
  })
);

// Start server.

SERVER.listen(PORT, () => console.log(`Listening on port ${PORT}.`));
