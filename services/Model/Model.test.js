const TestUtils = require('../../utils/TestUtils');
const Model = require('../../services/Model');
let User = require('../../models/User');

runTests();

async function runTests() {
  User = await TestUtils.mock(User);

  await TestUtils.test('Creating a user works', async () => {
    const UserService = User.toService();
    const testy = await UserService.create({
      email: 'test@example.com',
      username: 'Testy McTestface',
      password: 'top secret',
    });
    return typeof testy.id === 'string' && testy.id.length > 0;
  });

  await TestUtils.clearTable(User);

  await TestUtils.test('Email type properties are validated in create', async () => {
    const UserService = User.toService();
    try {
      await UserService.create({
        email: 'not a valid email',
        username: 'Testy McTestface',
        password: 'top secret',
      });
      return false;
    } catch (err) {
      return err.message === `Invalid email: 'not a valid email'`;
    }
  });

  await TestUtils.clearTable(User);

  await TestUtils.test('Email type properties are validated in update', async () => {
    const UserService = User.toService();
    const user = await UserService.create({
      email: 'valid@example.com',
      username: 'Testy McTestface',
      password: 'top secret',
    });
    try {
      await UserService.update(user, [['email', 'not a valid email']]);
      return false;
    } catch (err) {
      return err.message === `Invalid email: 'not a valid email'`;
    }
  });

  await TestUtils.clearTable(User);

  await TestUtils.test(
    'Creating a User where a unique property is set a to a value already taken throws an error',
    async () => {
      const UserService = User.toService();
      await UserService.create({
        email: 'alison@example.com',
        username: 'Alison Wonderland',
        password: 'top secret',
      });
      try {
        await UserService.create({
          email: 'alison@example.com',
          username: 'Bob Ross',
          password: 'happy little things',
        });
        return false;
      } catch (err) {
        return err.message === `Cannot set a unique property to a value that is already taken.`;
      }
    }
  );

  await TestUtils.clearTable(User);

  await TestUtils.test('Select all properties without conditions', async () => {
    const UserService = User.toService();
    await UserService.create({
      email: 'test@example.com',
      username: 'Testy McTestface',
      password: 'top secret',
    });
    const result = (await UserService.select()).pop();
    return typeof result.id === 'string' && result.id.length > 0;
  });

  await TestUtils.clearTable(User);

  await TestUtils.test('Select all properties with conditions', async () => {
    const UserService = User.toService();
    await UserService.create({
      email: 'test@example.com',
      username: 'Testy McTestface',
      password: 'dummy',
    });
    await UserService.create({
      email: 'bob@example.com',
      username: 'Bob Ross',
      password: 'dummy',
    });
    const result = await UserService.select({
      where: ['and', ['eq', 'email', 'bob@example.com'], ['eq', 'username', 'Bob Ross']],
    });
    const onlyReturnedOne = Array.isArray(result) && result.length === 1;
    const item = result.pop();
    return (
      onlyReturnedOne &&
      typeof item.id === 'string' &&
      item.id.length > 0 &&
      item.username === 'Bob Ross' &&
      item.email === 'bob@example.com'
    );
  });

  await TestUtils.clearTable(User);

  await TestUtils.test('Select some properties without conditions', async () => {
    const UserService = User.toService();
    await UserService.create({
      email: 'test@example.com',
      username: 'Testy McTestface',
      password: 'dummy',
    });
    await UserService.create({
      email: 'bob@example.com',
      username: 'Bob Ross',
      password: 'dummy',
    });
    const result = await UserService.select({ props: ['email'] });
    const item = result.pop();
    return (
      Array.isArray(result) &&
      result.length === 1 &&
      item.id === undefined &&
      item.username === undefined &&
      item.email === 'bob@example.com'
    );
  });

  await TestUtils.clearTable(User);

  await TestUtils.test(
    'Updating a private property should generate a new value, not use the given one.',
    async () => {
      const UserService = User.toService();
      const testy = await UserService.create({
        email: 'test@example.com',
        username: 'Testy McTestface',
        password: 'top secret',
      });
      const result = await UserService.update(testy, [['session_token']]);
      return (
        typeof result.session_token === 'string' &&
        result.session_token.length > 0 &&
        result.session_token !== testy.session_token
      );
    }
  );

  await TestUtils.clearTable(User);

  await TestUtils.test('Updating a normal property works', async () => {
    const UserService = User.toService();
    const testy = await UserService.create({
      email: 'test@example.com',
      username: 'Testy McTestface',
      password: 'top secret',
    });
    const result = await UserService.update(testy, [['email', 'different@example.com']]);
    return result.email === 'different@example.com';
  });

  await TestUtils.clearTable(User);

  await TestUtils.test(
    'Updating a unique property to a value already taken throws an error',
    async () => {
      const UserService = User.toService();
      const alison = await UserService.create({
        email: 'alison@example.com',
        username: 'Alison Wonderland',
        password: 'top secret',
      });
      await UserService.create({
        email: 'bob@example.com',
        username: 'Bob Ross',
        password: 'happy little things',
      });
      try {
        await UserService.update(alison, [['email', 'bob@example.com']]);
        return false;
      } catch (err) {
        return err.message === `Cannot set a value to a unique property that is already taken.`;
      }
    }
  );

  await TestUtils.clearTable(User);

  await TestUtils.test(
    'Updating a unique property to a value taken by the object being updated works',
    async () => {
      const UserService = User.toService();
      const alison = await UserService.create({
        email: 'alison@example.com',
        username: 'Alison Wonderland',
        password: 'top secret',
      });
      try {
        const result = await UserService.update(alison, [
          ['email', 'alison@example.com'],
          ['username', 'Alison Smith'],
        ]);
        return result.email === 'alison@example.com' && result.username === 'Alison Smith';
      } catch (err) {
        return false;
      }
    }
  );

  await TestUtils.clearTable(User);

  await TestUtils.test('Deleting a User works.', async () => {
    const UserService = User.toService();
    const alison = await UserService.create({
      email: 'alison@example.com',
      username: 'Alison Wonderland',
      password: 'top secret',
    });
    try {
      await UserService.delete(alison);
      const instances = await UserService.select();
      return instances.length === 0;
    } catch (err) {
      return false;
    }
  });

  await TestUtils.clearTable(User);

  await TestUtils.test('Properties may not be both private and readonly.', async () => {
    try {
      const TestModel = new Model({
        name: 'Widget',
        table: { name: 'test_widgets' },
        properties: [
          {
            name: 'id',
            type: 'uuid',
            private: true,
            readonly: true,
            primaryKey: true,
            unique: true,
          },
        ],
      });
      return false;
    } catch (err) {
      return (
        err.message ===
        `Properties cannot be both private and readonly. Property id on model Widget is the offender.`
      );
    }
  });

  await TestUtils.dropTable(User);
}
