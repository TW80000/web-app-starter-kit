const bcrypt = require('bcryptjs');
const express = require('express');
const { Pool } = require('pg');
const ModelService = require('../ModelService');
require('dotenv').config();

/**
 * An object with a rich set of features.
 */
class Model {
  constructor(model) {
    ModelService.validate(model);
    this.model = model;
    this.db = new Pool();
  }

  /**
   * Return a POJO version of the model.
   */
  toObject() {
    return JSON.parse(JSON.stringify(this.model));
  }

  /**
   * Dynamically create a service for a given model.
   */
  toService() {
    let DB = this.db;
    let model = this.model;
    return class AbstractService {
      constructor() {}

      /**
       * Get instances of the model from the database.
       * @param {Object} args Arguments. TODO: Document the arguments better.
       * @returns {Array<T>} An array of instances.
       */
      static async select(args) {
        if (args === undefined) return (await DB.query(`SELECT * FROM ${model.table.name}`)).rows;
        const propNames = args.props || '*';
        const where = args.where || [];
        const [conditions, values] = parsePredicate(where);
        let queryString = `SELECT ${propNames} FROM ${model.table.name}`;
        if (conditions.length > 0) queryString += ` WHERE ${conditions}`;
        return (await DB.query(queryString, values)).rows;
      }

      /**
       * Create a new instance of this model, save it in the database, and return it.
       *
       * Features:
       *
       *   - An error will be thrown if you have not provided a value for a required property.
       *   - An error will be thrown if you try to set a unique property where that value is already
       *     taken by another instance.
       *   - Properties of type 'password' will be hashed before being stored.
       *   - Read-only properties will have values generated for them.
       *
       * @param {Object} obj An object where keys represent properties on the
       * model and values represent values for those properties.
       */
      static async create(obj) {
        if (
          model.properties
            .filter(property => property.readonly)
            .some(property => property.readonly && obj.hasOwnProperty(property.name))
        ) {
          throw new Error(`Cannot set read-only property: ${property.name}`);
        }

        // Check unique properties.
        await checkForUniquePropertyViolation.call(this, model, obj);

        const values = model.properties
          .filter(prop => !prop.readonly && !prop.private)
          .map(property => {
            if (property.required && !obj.hasOwnProperty(property.name)) {
              throw new Error(`Property ${property.name} is required.`);
            } else if (property.type === 'password') {
              return bcrypt.hashSync(obj[property.name], bcrypt.genSaltSync(10));
            } else if (property.type === 'email' && !isValidEmail(obj[property.name])) {
              throw new Error(`Invalid email: '${obj[property.name]}'`);
            }
            return obj[property.name];
          });
        var i = 1;
        const placeholders = model.properties
          .filter(prop => !prop.readonly)
          .map(prop => (prop.private ? defaultValueFunctionFor(prop.type) : `$${i++}`))
          .join(', ');
        var propNames = model.properties
          .filter(property => !property.readonly)
          .map(property => property.name)
          .join(', ');
        var queryString = `INSERT INTO ${
          model.table.name
        }(${propNames}) VALUES (${placeholders}) RETURNING *`;
        return (await DB.query(queryString, values)).rows.pop();
      }

      /**
       * Updates the value of a property on an instance of the model.
       * @param {Instance} existing An existing instance of the model.
       * @param {Array} updates TODO
       */
      static async update(existing, updates) {
        if (!Array.isArray(updates)) throw new Error('You must pass update an array.');
        if (updates.length === 0) throw new Error();

        const updateObj = updates
          .filter(updateArr => updateArr.length === 2)
          .reduce((acc, updateArr) => {
            acc[updateArr[0]] = updateArr[1];
            return acc;
          }, {});
        const propsToSet = model.properties.filter(
          prop =>
            prop.unique &&
            updateObj.hasOwnProperty(prop.name) &&
            updateObj[prop.name] !== existing[prop.name]
        );
        if (propsToSet.length > 0) {
          const conditions = propsToSet.reduce(
            (acc, prop) => acc.concat([['eq', prop.name, updateObj[prop.name]]]),
            ['or']
          );
          const existingInstances = await this.select({ where: conditions });
          if (existingInstances.length > 0) {
            throw new Error(`Cannot set a value to a unique property that is already taken.`);
          }
        }

        const values = [];
        const assignments = [];
        let i = 1;

        updates.forEach(arr => {
          const propName = arr.shift();
          const property = model.properties.find(property => property.name === propName);

          if (property === undefined) {
            throw new Error(`Model ${model.name} doesn't have a property called ${propName}.`);
          } else if (property.private) {
            if (arr.length) {
              throw new Error(
                `Cannot update read-only property ${property.name} on model ${model.name}.`
              );
            }
            assignments.push(`${propName} = ${defaultValueFunctionFor(property.type)}`);
          } else if (property.readonly) {
            throw new Error(`Cannot set a readonly property.`);
          } else if (property.type === 'email' && !isValidEmail(arr[0])) {
            throw new Error(`Invalid email: '${arr[0]}'`);
          } else {
            let value = arr.shift();
            if (property.type === 'password') {
              value = bcrypt.hashSync(value, bcrypt.genSaltSync(10));
            }
            values.push(value);
            assignments.push(`${propName} = $${i++}`);
          }
        });

        // TODO: This is done elsewhere, can probably be refactored.
        const primaryKeyProps = model.properties.filter(property => property.primaryKey);
        const identityCondition = primaryKeyProps
          .map(prop => {
            values.push(existing[prop.name]);
            return `${prop.name} = $${i++}`;
          })
          .join(' AND ');
        const queryString = `UPDATE ${model.table.name} SET ${assignments.join(
          ', '
        )} WHERE ${identityCondition} RETURNING *`;

        return (await DB.query(queryString, values)).rows.pop();
      }

      /**
       * Delete an instance from the database.
       * @param {Instance<Model>} existing An existing instance of the model to delete. Only needs
       * to be an object with the primary keys and values.
       */
      static async delete(existing) {
        const values = [];
        let i = 1;
        const primaryKeyProps = model.properties.filter(property => property.primaryKey);
        const identityCondition = primaryKeyProps
          .map(prop => {
            values.push(existing[prop.name]);
            return `${prop.name} = $${i++}`;
          })
          .join(' AND ');
        const queryString = `DELETE FROM ${model.table.name} WHERE ${identityCondition}`;
        await DB.query(queryString, values);
      }
    };
  }

  /**
   * Generate an SQL script to create a table to store instances of the given
   * model.
   */
  toSQL() {
    let model = this.model;
    const propertyDefs = model.properties.map(property => {
      let line = `${property.name} ${getSqlTypeForPropertyType(property.type)}`;
      if (property.readonly) {
        line += ' DEFAULT ' + defaultValueFunctionFor(property.type);
      }
      if (property.type === 'reference') {
        const referenceModel = require(`../../models/${property.referenceModel}`).model;
        line += ` REFERENCES ${referenceModel.table.name}(${property.referenceProperty})`;
      }
      return line;
    });
    const primaryKeys = model.properties
      .filter(property => property.primaryKey)
      .map(property => property.name);
    var output = `CREATE TABLE ${model.table.name} (
\t${propertyDefs.join(',\n\t')},
\tPRIMARY KEY (${primaryKeys.join(', ')})
);`;
    return output;
  }

  /**
   * Create an express router for a given model that returns JSON.
   */
  toJsonRouter() {
    let model = this.model;
    const service = this.toService(model);
    const router = express.Router();
    router.get(`/`, async (req, res) => {
      try {
        const instances = await service.select({
          props: model.properties
            .filter(prop => !(prop.private || prop.type === 'password'))
            .map(prop => prop.name),
        });
        return res.json(instances);
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err,
        });
        console.error(err);
      }
    });
    return router;
  }

  /**
   * Returns an Express router with routes that return HTML views for displaying and interacting
   * with instances of the model.
   */
  toViewRouter() {
    let DB = this.db;
    const model = this.model;
    const service = this.toService();
    const router = express.Router();

    router.get('/detail', async (req, res) => {
      await populateReferencePropertyOptions(model);

      // We identify a unique record by a combination of its primary keys.
      const primaryKeys = model.properties.filter(prop => prop.primaryKey);
      const identityConditions = primaryKeys.map(prop => ['eq', prop.name, req.query[prop.name]]);

      try {
        const instances = await service.select({ where: ['and'].concat(identityConditions) });
        const instance = instances.pop();
        if (instance === undefined) throw new Error('Instance not found.');
        res.render('core/detail-page', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          instance: instance,
          model: model,
        });
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err,
        });
        console.error(err);
      }
    });

    router.get('/table', async (req, res) => {
      try {
        await populateReferencePropertyOptions(model);
        const instances = await service.select();
        res.render('core/table-page', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          instances: instances,
          model: model,
        });
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err,
        });
        console.error(err);
      }
    });

    router.get('/edit', async (req, res) => {
      // We identify a unique record by a combination of its primary keys.
      const primaryKeys = model.properties.filter(prop => prop.primaryKey);
      const identityConditions = primaryKeys.map(prop => ['eq', prop.name, req.query[prop.name]]);

      await populateReferencePropertyOptions(model);

      try {
        const instances = await service.select({ where: ['and'].concat(identityConditions) });
        const instance = instances.pop();
        if (instance === undefined) throw new Error('Instance not found.');

        res.render('core/editor-page', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          instance: instance,
          model: model,
          buttonLabel: 'Update',
        });
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err,
        });
        console.error(err);
      }
    });

    router.post('/edit', async (req, res) => {
      try {
        // We identify a unique record by a combination of its primary keys.
        const primaryKeys = model.properties.filter(prop => prop.primaryKey);
        const identity = primaryKeys.reduce((acc, prop) => {
          acc.push(['eq', prop.name, req.query[prop.name]]);
          return acc;
        }, []);

        // Get the instance to update.
        const instance = (await service.select({ where: ['and'].concat(identity) })).pop();
        if (instance === undefined) throw new Error('Instance not found.');

        // The user can update any property that isn't private or readonly.
        const settableProperties = model.properties.filter(prop => !prop.readonly && !prop.private);
        const updates = settableProperties.reduce((acc, prop) => {
          acc.push([prop.name, req.body[prop.name]]);
          return acc;
        }, []);

        // Update the instance.
        const updatedInstance = await service.update(instance, updates);

        res.render('core/editor-page', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          instance: updatedInstance,
          model: model,
          banner: {
            message: `Successfully updated ${model.name}.`,
            type: 'success',
          },
        });
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err.message,
        });
        console.error(err);
      }
    });

    router.get('/create', async (req, res) => {
      try {
        await populateReferencePropertyOptions(model);
        res.render('core/editor-page', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          model: model,
          buttonLabel: 'Create',
        });
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err,
        });
        console.error(err);
      }
    });

    router.post('/create', async (req, res) => {
      try {
        // Create the instance.
        const instance = await service.create(req.body);

        // We identify a unique record by a combination of its primary keys.
        const primaryKeys = model.properties.filter(prop => prop.primaryKey);

        const query = primaryKeys
          .map(prop => `${prop.name}=${encodeURIComponent(instance[prop.name])}`)
          .join('&');

        res.redirect(`./detail?${query}`);
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err.message,
        });
        console.error(err);
      }
    });

    router.get('/controller', async (req, res) => {
      try {
        await populateReferencePropertyOptions(model);
        const instances = await service.select();
        res.render('core/controller-page', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          instances: instances,
          model: model,
        });
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err,
        });
        console.error(err);
      }
    });

    router.get('/delete', async (req, res) => {
      // We identify a unique record by a combination of its primary keys.
      const primaryKeys = model.properties.filter(prop => prop.primaryKey);
      const identity = primaryKeys.reduce((acc, prop) => {
        acc.push(['eq', prop.name, req.query[prop.name]]);
        return acc;
      }, []);

      await populateReferencePropertyOptions(model);

      // Get the instance to update.
      const instance = (await service.select({ where: ['and'].concat(identity) })).pop();
      if (instance === undefined) throw new Error('Instance not found.');

      try {
        res.render('core/delete-page', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          instance: instance,
          model: model,
        });
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err,
        });
        console.error(err);
      }
    });

    router.post('/delete', async (req, res) => {
      try {
        // We identify a unique record by a combination of its primary keys.
        const primaryKeys = model.properties.filter(prop => prop.primaryKey);
        const identity = primaryKeys.reduce((acc, prop) => {
          acc.push(['eq', prop.name, req.query[prop.name]]);
          return acc;
        }, []);

        // Get the instance to update.
        const instance = (await service.select({ where: ['and'].concat(identity) })).pop();
        if (instance === undefined) throw new Error('Instance not found.');

        // Delete the instance.
        await service.delete(instance);

        res.render('core/delete-page', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          model: model,
          banner: {
            message: `Successfully deleted ${model.name}.`,
            type: 'success',
          },
        });
      } catch (err) {
        res.render('error', {
          base: process.env.base,
          isAuthenticated: req.isAuthenticated,
          message: err.message,
        });
        console.error(err);
      }
    });

    return router;
  }
}

function parsePredicate(structure) {
  const values = [];
  if (!Array.isArray(structure)) throw new Error(`You must pass an array to parsePredicate.`);
  if (structure.length === 0) return ['', values];
  var i = 1;
  const parseCondition = arr => {
    const predicate = arr.shift();
    switch (predicate) {
      case 'and':
        return `(${arr.map(parseCondition).join(' AND ')})`;
      case 'or':
        return `(${arr.map(parseCondition).join(' OR ')})`;
      case 'eq':
        const propertyName = arr.shift();
        values.push(arr.shift());
        return `(${propertyName} = $${i++})`;
      default:
        throw new Error(`Unrecognized predicate: ${predicate}`);
    }
  };
  return [parseCondition(structure.slice()), values];
}

/**
 * Check if the given model has any properties of type reference. If so, populate the 'options'
 * attribute on those properties so that the 'formInput' view can give a list of options to select
 * from when setting that reference property.
 *
 * Modifies the argument by reference.
 *
 * NOTE: I still don't know if this is a good idea.
 *
 * @param {Model} model A model.
 */
async function populateReferencePropertyOptions(model) {
  const referenceProps = model.properties.filter(prop => prop.type === 'reference');
  await Promise.all(
    referenceProps.map(prop => {
      const referenceModelService = require(`../../models/${prop.referenceModel}`).toService();
      return referenceModelService.select().then(value => (prop.options = value));
    })
  );
}

/**
 * Return the associated SQL data type for a given property type.
 * @param {String} type A property type.
 * @private
 */
function getSqlTypeForPropertyType(type) {
  switch (type) {
    case 'password':
    case 'email':
    case 'string':
      return 'varchar';
    case 'uuid':
      return 'uuid';
    case 'date':
      return 'timestamp';
    case 'number':
      return 'double precision';
    case 'reference':
      // Should models be able to specify their id property and type? For now just assume it will
      // always be uuid.
      return 'uuid';
    default:
      throw new Error(`No SQL type set for properties of type '${type}'.`);
  }
}

/**
 * Given a type, return a Postgres built-in function that will generate a default value for that
 * data type.
 * @param {String} type A type.
 * @private
 */
function defaultValueFunctionFor(type) {
  switch (type) {
    case 'uuid':
      return 'uuid_generate_v4()';
    case 'date':
      return 'clock_timestamp()';
    default:
      throw new Error(
        `Don't know how to generate a default value for a property of type ${property.type}.`
      );
  }
}

/**
 * Check if the given instance of a model has a unique property with a value that's already taken.
 * @param {Model} model A model.
 * @param {Object} obj An instance of the model.
 */
async function checkForUniquePropertyViolation(model, obj) {
  const propsToSet = model.properties.filter(prop => prop.unique && obj.hasOwnProperty(prop.name));
  if (propsToSet.length === 0) return;
  const conditions = propsToSet.reduce(
    (acc, prop) => acc.concat([['eq', prop.name, obj[prop.name]]]),
    ['or']
  );
  const existingInstances = await this.select({ where: conditions });
  if (existingInstances.length > 0) {
    throw new Error(`Cannot set a unique property to a value that is already taken.`);
  }
}

/**
 * Checks if a string is a valid email address.
 * @param {String} str A string.
 * @returns {Boolean} True if the given string is an email address.
 */
function isValidEmail(str) {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    str
  );
}

module.exports = Model;
