/**
 * A utility class for working on models.
 */
class ModelService {
  constructor() {}

  /**
   * Check that a model is valid.
   * @param {model} model A model to validate.
   * @private
   */
  static validate(model) {
    if (model === undefined) {
      throw new Error(`Model cannot be undefined.`);
    }

    if (!typeof model.name === 'string' || model.name.length === 0) {
      throw new Error(`Model must have a name.`);
    }

    if (!typeof model.table.name === 'string' || model.table.name.length === 0) {
      throw new Error(`Model ${model.name} must have a table name.`);
    }

    if (!Array.isArray(model.properties)) {
      throw new Error(`Model ${model.name} must have properties.`);
    }

    model.properties.forEach(prop => {
      if (prop.name !== prop.name.toLowerCase()) {
        throw new Error(
          `Properties cannot contain uppercase letters. Property ${prop.name} on model ${
            model.name
          } is the offender.`
        );
      } else if (prop.readonly && prop.private) {
        throw new Error(
          `Properties cannot be both private and readonly. Property ${prop.name} on model ${
            model.name
          } is the offender.`
        );
      } else if (prop.type === 'reference') {
        if (!prop.hasOwnProperty('referenceModel')) {
          throw new Error(
            `Property ${prop.name} is of type reference but did not set the referenceModel.`
          );
        } else if (!prop.hasOwnProperty('referenceProperty')) {
          throw new Error(
            `Property ${prop.name} is of type reference but did not set the referenceProperty.`
          );
        }

        let referenceModel;
        try {
          referenceModel = require(`../../models/${prop.referenceModel}`).model;
        } catch (err) {
          throw new Error(
            `Property ${prop.name} references model ${
              prop.referenceModel
            } but that model doesn't exist.`
          );
        }

        const hasProperty = (name, model) =>
          model.properties.some(property => property.name === name);

        if (!hasProperty(prop.referenceProperty, referenceModel)) {
          throw new Error(
            `Property ${prop.name} references property ${prop.referenceProperty} on model ${
              prop.referenceModel
            } but that property doesn't exist on ${prop.referenceModel}.`
          );
        }
      }
    });

    if (!model.properties.some(property => property.primaryKey)) {
      throw new Error(`Model ${model.name} must have at least one primary key.`);
    }

    // TODO: Validate properties.
  }
}

module.exports = ModelService;
