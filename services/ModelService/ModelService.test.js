const TestUtils = require('../../utils/TestUtils');
const ModelService = require('./ModelService');
const User = require('../../models/User');

TestUtils.test('Validation throws if any property has an uppercase letter', async () => {
  const BadModel = {
    name: 'BadModel',
    table: { name: 'bad_models' },
    properties: [{ name: 'somethingWithACapitalInIt' }],
  };
  try {
    ModelService.validate(BadModel);
    return false;
  } catch (err) {
    return (
      err.message ===
      `Properties cannot contain uppercase letters. Property somethingWithACapitalInIt on model BadModel is the offender.`
    );
  }
});
