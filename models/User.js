const Model = require('../services/Model');

module.exports = new Model({
  name: 'User',
  table: {
    name: 'users',
  },
  properties: [
    {
      name: 'id',
      type: 'uuid',
      primaryKey: true,
      private: true,
    },
    {
      name: 'session_token',
      type: 'uuid',
      unique: true,
      private: true,
    },
    {
      name: 'username',
      displayName: 'Username',
      type: 'string',
      required: true,
      unique: true,
    },
    {
      name: 'email',
      displayName: 'Email',
      type: 'email',
      required: true,
      unique: true,
    },
    {
      name: 'password',
      displayName: 'Password',
      type: 'password',
      required: true,
    },
    {
      name: 'joined',
      displayName: 'Date Joined',
      type: 'date',
      readonly: true,
    },
  ],
});
