const express = require('express');
const User = require('../models/User');
const UserService = User.toService();

const router = express.Router();

router.get('/', async (req, res) => {
  return res.render('register', {
    base: process.env.base,
    isAuthenticated: req.isAuthenticated,
  });
});

router.post('/', async (req, res) => {
  try {
    if (
      !req.body.hasOwnProperty('username') ||
      !(typeof req.body.username === 'string') ||
      !(req.body.username.length > 0)
    ) {
      // No username
      return res.render('register', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'Username cannot be blank.',
          type: 'error',
        },
      });
    }

    if (
      !req.body.hasOwnProperty('password') ||
      !(typeof req.body.password === 'string') ||
      !(req.body.password.length > 0)
    ) {
      // No password
      return res.render('register', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'Password cannot be blank.',
          type: 'error',
        },
      });
    }

    // Username already taken
    let user = (await UserService.select('username', req.body.username)).pop();
    if (user !== undefined) {
      return res.render('register', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'That username is already taken.',
          type: 'error',
        },
      });
    }

    // Email already taken
    user = (await UserService.select('email', req.body.email)).pop();
    if (user !== undefined) {
      return res.render('register', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'That email address already belongs to an existing account.',
          type: 'error',
        },
      });
    }

    // Create new user
    user = await UserService.create({
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
    });

    // Happy path, login successful
    return res
      .status(200)
      .cookie('username', user.username)
      .cookie('token', user.session_token)
      .redirect('/');
  } catch (err) {
    console.error(err);
    return res.render('register', {
      base: process.env.base,
      isAuthenticated: req.isAuthenticated,
      banner: {
        message: 'Registration failed due to server error: ' + err.message,
        type: 'error',
      },
    });
  }
});

module.exports = router;
