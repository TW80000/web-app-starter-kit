const express = require('express');
const User = require('../models/User');
const UserService = User.toService();
const bcrypt = require('bcryptjs');

const router = express.Router();

router.get('/', (req, res) => {
  return res.render('login', {
    redirectTo: req.query.redirectTo,
    base: process.env.base,
    isAuthenticated: req.isAuthenticated,
  });
});

router.post('/', async (req, res) => {
  try {
    if (
      !req.body.hasOwnProperty('username') ||
      !(typeof req.body.username === 'string') ||
      !(req.body.username.length > 0)
    ) {
      // No username
      return res.render('login', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'Must provide username',
          type: 'error',
        },
      });
    }

    if (
      !req.body.hasOwnProperty('password') ||
      !(typeof req.body.password === 'string') ||
      !(req.body.password.length > 0)
    ) {
      // No password
      return res.render('login', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'Must provide password',
          type: 'error',
        },
      });
    }

    // User doesn't exist
    let user = (await UserService.select('username', req.body.username)).pop();
    if (user === undefined) {
      return res.render('login', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'User not found',
          type: 'error',
        },
      });
    }

    // Incorrect password
    if (!(await bcrypt.compare(req.body.password, user.password))) {
      return res.render('login', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'Incorrect password',
          type: 'error',
        },
      });
    }

    // Happy path, login successful
    user = await UserService.update(user, [['session_token']]);
    const redirectTo = req.body.redirectTo || '/';
    return res
      .status(200)
      .cookie('token', user.session_token)
      .cookie('username', user.username)
      .redirect(`.${redirectTo}`);
  } catch (err) {
    console.error(err);
    return res.render('login', {
      base: process.env.base,
      isAuthenticated: req.isAuthenticated,
      banner: {
        message: 'Login failed due to server error: ' + err.message,
        type: 'error',
      },
    });
  }
});

module.exports = router;
