const express = require('express');
const User = require('../models/User');
const UserService = User.toService();

const router = express.Router();

router.get('/', async (req, res) => {
  if (req.cookies.username) {
    const user = (await UserService.select('username', req.cookies.username)).pop();
    if (user) UserService.update(user, [['session_token']]);
  }
  res.clearCookie('username');
  res.clearCookie('token');
  res.redirect('./?logout=true');
});

module.exports = router;
